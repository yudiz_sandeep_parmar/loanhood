class ServerController {
	async server(req, res) {
		try {
			return res.json({ message: 'Welcome to Loan App!' })
		} catch (error) {
			return res.status(404).json({ message: error.message })
		}
	}

	async wildCard(req, res) {
		try {
			return res.json({ message: 'Route not found!' })
		} catch (error) {
			return res.status(404).json({ message: error.message })
		}
	}
}
module.exports = new ServerController()
