const {
	Rentals,
	Rentalimages,
	Rentaltransactions,
	Reports,
	Likes,
	User,
	Messages,
	Rentalitems,
	Brands,
	Categories,
	Subcategories,
	Sizes,
	Follows,
} = require('../models/')
const { Op, Sequelize, fn, col } = require('sequelize')
class Controller {
	async RentalImagesLoaner(req, res) {
		//Need a list of rentals with their rental images and with their loaner.
		try {
			const data = await Rentals.findAll({
				include: [{ model: Rentalimages, where: { status: true } }],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}

	//Need a list of users with their reported users.
	async userListWithReportedUsers(req, res) {
		try {
			const data = await User.findAll({
				attributes: ['id', 'firstName', 'userName'],
				include: [
					{
						model: Reports,
						as: 'reporter',
						required: true,
						attributes: ['id', 'userId', 'reportedUserId', 'text'],
						include: [
							{ model: User, required: true, as: 'reportedUsers', attributes: ['id', 'firstName', 'userName'] },
						],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}

	//Need a list of rentals with the reported user
	async reportedUsersRentals(req, res) {
		try {
			const data = await Rentals.findOne({
				where: { id: 4 },
				attributes: ['id', 'userId', 'title'],
				include: [
					{
						model: User,
						as: 'userRental',
						required: true,
						attributes: ['id', 'firstName', 'userName'],
						include: [{ model: Reports, as: 'reporter', attributes: ['id', 'userId', 'reportedUserId'] }],
					},
				],
			})
			// const data = await Reports.findAll()
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}

	//Need a list of chat messages with all the loaner/borrower considering you are the logged in user and getting the chat dashboard with the latest message.
	async messageDashboard(req, res) {
		try {
			const data = await Rentaltransactions.findAndCountAll({
				attributes: ['id', 'loanerId', 'borrowerId'],
				where: { [Op.or]: [{ loanerId: 39 }, { borrowerId: 39 }] },
				include: [
					{
						model: Messages,
						required: true,
						order: [[Messages, 'createdAt', 'DESC']],
						attributes: ['id', 'rentalTransactionId', 'senderId', 'receiverId', 'message', 'createdAt', 'updatedAt'],
						where: { key: 'chat' },
						include: [
							{ model: User, required: true, as: 'sender', attributes: ['id', 'firstName', 'userName'] },
							{ model: User, required: true, as: 'receiver', attributes: ['id', 'firstName', 'userName'] },
						],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}

	//Need chat messages of a particular chat between loaner and a borrower with the detail of loaner borrower and the rental.
	async chatWithRental(req, res) {
		try {
			const data = await Messages.findAll({
				required: true,
				where: { key: 'chat', [Op.or]: [{ senderId: 39 }, { receiverId: 39 }], rentalTransactionId: 94 },
				order: [['createdAt', 'DESC']],
				attributes: ['id', 'rentalTransactionId', 'senderId', 'receiverId', 'message', 'createdAt', 'updatedAt'],
				include: [
					{
						model: Rentaltransactions,
						required: true,
						as: 'messageWithRentalTransaction',
						attributes: ['id', 'rentalId', 'loanerId', 'borrowerId'],
						include: [
							{
								model: User,
								required: true,
								as: 'loaner',
								attributes: ['id', 'firstName', 'userName'],
							},
							{
								model: User,
								required: true,
								as: 'borrower',
								attributes: ['id', 'firstName', 'userName'],
							},
							{ model: Rentals, attributes: ['id', 'userId', 'title'] },
						],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}

	//Get a single Rental with rental items, images and with their Loaner.
	async rentalWithItemsLoanerAndImages(req, res) {
		try {
			const data = await Rentals.findAll({
				where: { id: 6 },
				include: [
					{ model: Rentalitems },
					{ model: Rentalimages },
					{ model: User, required: true, attributes: ['id', 'userName'] },
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}

	//Need a list of rentals with their rental transactions, their loaner and borrower.
	async rentalLoanerWithTransactionsAndBorrower(req, res) {
		try {
			const data = await Rentals.findAll({
				include: [
					{
						model: Rentaltransactions,
						include: [
							{ model: User, required: true, as: 'loaner', attributes: ['id', 'userName'] },
							{ model: User, required: true, as: 'borrower', attributes: ['id', 'userName'] },
						],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}

	//Get a rental with their loaner and their rental transactions.
	async rentalLoanerWithTransactions(req, res) {
		try {
			const data = await Rentals.findOne({
				where: { id: 15 },
				include: [{ model: User, required: true }, { model: Rentaltransactions }],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}

	//Need a list of rentals with their rental transactions and their rental transactions states.
	async rentalTransactionWithState(req, res) {
		try {
			const data = await Rentals.findAll({
				include: [{ model: Rentaltransactions, include: [{ model: Rentaltransactionstates }] }],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}

	//Get a rental with their meta data (brands, categories, subcategories, size etc.)
	async rentalMetadata(req, res) {
		try {
			const data = await Rentals.findOne({
				include: [
					{
						model: Rentalitems,
						include: [{ model: Brands }, { model: Categories }, { model: Subcategories }, { model: Sizes }],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}

	//Need a list of likes of a rental with the users who liked it.
	async rentalLikes(req, res) {
		try {
			const data = await Rentals.findAll({
				attributes: ['id', 'userId', 'title'],
				include: [
					{ model: Likes, required: true, include: [{ model: User, required: true, attributes: ['id', 'firstName'] }] },
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}
	//Get all the details about a rental like who is the loaner, how many borrowers with their details, price, what is the rental transaction state, what is the latest state.
	async rentalDetails(req, res) {
		try {
			const count = await Rentals.findOne({
				where: { id: 5 },
				include: [
					{
						model: Rentaltransactions,
						attributes: [],
					},
				],
				attributes: [[fn('COUNT', col('rentalTransactions.id')), 'borrowerCount']],
				group: ['rentals.id'],
			})
			const data = await Rentals.findOne({
				where: { id: 5 },
				attributes: ['id', 'userId', 'title'],
				include: [
					{ model: User, required: true, as: 'userRental', attributes: ['id', 'userName', 'firstName', 'lastName'] },
					{
						model: Rentaltransactions,
						attributes: ['id', 'rentalId', 'borrowerId', 'currentStatus'],
						include: [
							{
								model: User,
								required: true,
								as: 'borrower',
								attributes: ['id', 'userName', 'firstName', 'lastName'],
							},
						],
					},
				],
			})
			return res.status(200).json({ count, data })
		} catch (e) {
			return res.status(500).json({ message: e.message })
		}
	}

	//Need a list of followers with their rentals, images and rental items, and the blocked users and who blocked you users won’t be shown in the list
	async followerList(req, res) {
		try {
			const data = await User.findAll({
				where: { id: 42 },
				attributes: ['id', 'firstName', 'userName'],
				include: [
					{ model: Follows, as: 'follower' },
					{
						model: Rentals,
						attributes: ['id', 'userId', 'title'],
						as: 'userRental',
						include: [{ model: Rentalimages }, { model: Rentalitems }],
					},
					{
						model: Reports,
						as: 'reporter',
						where: { isBlocked: true },
						include: [{ model: User, as: 'reportedUsers', attributes: ['id', 'firstName', 'userName'] }],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}
	//Need a list of following with their rentals, images and rental items, and the blocked users and who blocked you users won’t be shown in the list
	async followingList(req, res) {
		try {
			const data = await User.findAll({
				where: { id: 42 },
				attributes: ['id', 'firstName', 'userName'],
				include: [
					{ model: Follows, as: 'following' },
					{
						model: Rentals,
						attributes: ['id', 'userId', 'title'],
						as: 'userRental',
						include: [{ model: Rentalimages }, { model: Rentalitems }],
					},
					{
						model: Reports,
						as: 'reporter',
						where: { isBlocked: true },
						include: [{ model: User, as: 'reportedUsers', attributes: ['id', 'firstName', 'userName'] }],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}

	//Need a list of users reported by a particular user(you) considering you are the logged in user.
	async reportedUserList(req, res) {
		try {
			const data = await User.findAll({
				where: { id: 42 },
				attributes: ['id', 'firstName', 'lastName'],
				include: [{ model: Reports, as: 'reporter' }],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}
	//Need a list of users who reported you with the reason.
	async reportedUserListWithReason(req, res) {
		try {
			const data = await User.findAll({
				attributes: ['id', 'firstName', 'lastName'],
				include: [
					{
						model: Reports,
						as: 'reporter',
						include: [{ model: User, as: 'reportedUsers', attributes: ['id', 'firstName', 'lastName'] }],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}

	//Need a list of user who reported the rental.
	async rentalReports(req, res) {
		try {
			const data = await User.findAll({
				attributes: ['id', 'firstName', 'lastName'],
				include: [
					{
						model: Reports,
						as: 'reportedUsers',
						include: [{ model: User, as: 'reporter', attributes: ['id', 'firstName', 'lastName'] }],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}

	//Need a list of users who blocked you with the user’s rental.
	async userBlockedList(req, res) {
		try {
			const data = await User.findAll({
				attributes: ['id', 'firstName', 'lastName'],
				include: [
					{
						model: Reports,
						as: 'reporter',
						where: { isBlocked: true },
						include: [
							{ model: User, as: 'reportedUsers', attributes: ['id', 'firstName', 'lastName'] },
							{ model: Rentals, as: 'rentalReport', attributes: ['id', 'userId', 'title'] },
						],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}
	//Need a list of users who you blocked with the user’s rental
	async rentalBlockedByUser(req, res) {
		try {
			const data = await User.findAll({
				attributes: ['id', 'firstName', 'lastName'],
				include: [
					{
						model: Reports,
						as: 'reportedUsers',
						where: { isBlocked: true },
						include: [
							{ model: User, as: 'reporter', attributes: ['id', 'firstName', 'lastName'] },
							{ model: Rentals, as: 'rentalReport', attributes: ['id', 'userId', 'title'] },
						],
					},
				],
			})
			return res.status(200).json({ data })
		} catch (e) {
			return res.status(500).json(e.message)
		}
	}
}
module.exports = new Controller()
