const router = require('express').Router()
const Controller = require('../Controllers/Controller')
router.get('/rentalImageLoner', Controller.RentalImagesLoaner)
router.get('/rentalLikes', Controller.rentalLikes)
router.get('/userListWithReportedUsers', Controller.userListWithReportedUsers)
router.get('/followingList', Controller.followingList)
router.get('/reportedUserList', Controller.reportedUserList)
router.get('/reportedUserListWithReason', Controller.reportedUserListWithReason)
router.get('/rentalReports', Controller.rentalReports)
router.get('/userBlockedList', Controller.userBlockedList)
router.get('/rentalBlockedByUser', Controller.rentalBlockedByUser)
router.get('/rentalDetails', Controller.rentalDetails)

module.exports = router
