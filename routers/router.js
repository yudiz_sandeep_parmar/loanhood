const router = require('express').Router()
const ServerController = require('../Controllers/ServerController')
const loanRouter = require('./loanRouter')

router.use(loanRouter)
router.get('/', ServerController.server)
router.all('*', ServerController.wildCard)
module.exports = router
