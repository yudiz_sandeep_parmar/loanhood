const { Sequelize } = require('sequelize')

const sequelize = new Sequelize('loanhood', process.env.SQL_USER, process.env.SQL_PASSWORD, {
	host: process.env.SQL_URL,
	dialect: 'postgres',
	pool: {
		max: 1,
		min: 0,
		acquire: 10000,
		idle: 15000,
	},
})
try {
	sequelize.authenticate()
	console.log('Connection has been established successfully.')
} catch (error) {
	console.error('Unable to connect to the database:', error)
}

module.exports = { sequelize }
