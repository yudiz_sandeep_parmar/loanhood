const Sequelize = require('sequelize')
const { sequelize } = require('../../db/sequelize')

const likes = sequelize.define('likes', {
	userId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'users',
			key: 'id',
		},
		allowNull: false,
	},
	rentalId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'rentals',
			key: 'id',
		},
		allowNull: false,
	},
	status: {
		type: Sequelize.BOOLEAN,
		defaultValue: true,
	},
})

likes.sync()

module.exports = likes
