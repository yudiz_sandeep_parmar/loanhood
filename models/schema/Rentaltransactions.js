const Sequelize = require('sequelize')
const { sequelize } = require('../../db/sequelize')

const values = [
	'requestPending',
	'requestDeclined',
	'requestCanceled',
	'requestAccepted',
	'requestTimeout',
	'paymentPending',
	'paymentAuthorized',
	'paymentTimeout',
	'paymentFailed',
	'paymentSuccess',
	'confirmed',
	'deliveryWaitingForLoaner',
	'deliveryShippedByLoaner',
	'inPersonCollectionPending',
	'receivedByBorrower',
	'extensionRequestPending',
	'extensionRequestDeclined',
	'extensionRequestCanceled',
	'extensionRequestAccepted',
	'extensionPaymentPending',
	'extensionPaymentFailed',
	'extensionPaymentSuccess',
	'extensionConfirmed',
	'returnWaitingForBorrower',
	'returnShippedByBorrower',
	'inPersonReturnPending',
	'receivedByLoaner',
	'completed',
]

const rentalTransactions = sequelize.define('rentalTransactions', {
	borrowerAddressId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'addresses',
			key: 'id',
		},
	},
	loanerAddressId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'addresses',
			key: 'id',
		},
	},
	rentalId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'rentals',
			key: 'id',
		},
		allowNull: false,
	},
	loanerId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'users',
			key: 'id',
		},
		allowNull: false,
	},
	borrowerId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'users',
			key: 'id',
		},
		allowNull: false,
	},
	startAt: {
		type: Sequelize.DATEONLY,
		allowNull: false,
		defaultValue: new Date(),
	},
	endAt: {
		type: Sequelize.DATEONLY,
		allowNull: false,
	},
	deliveryPrice: {
		type: Sequelize.FLOAT,
	},
	isPickup: {
		type: Sequelize.BOOLEAN,
		defaultValue: false,
	},
	dryCleanPrice: {
		type: Sequelize.FLOAT,
	},
	stripePaymentId: {
		type: Sequelize.STRING,
	},
	isPaymentSuccess: {
		type: Sequelize.BOOLEAN,
		defaultValue: false,
	},
	isUpdated: {
		type: Sequelize.BOOLEAN,
		defaultValue: false,
	},
	currentStatus: {
		type: Sequelize.ENUM,
		defaultValue: 'requestPending',
		values,
	},
	status: {
		type: Sequelize.BOOLEAN,
		defaultValue: true,
	},
	isDeliveryAddressConfirmed: {
		type: Sequelize.BOOLEAN,
		defaultValue: false,
	},
	reasonForCancelation: {
		type: Sequelize.STRING,
	},
})

rentalTransactions.sync()

module.exports = rentalTransactions
