const Sequelize = require('sequelize')
const { sequelize } = require('../../db/sequelize')

const brands = sequelize.define('brands', {
	name: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	status: {
		type: Sequelize.BOOLEAN,
		defaultValue: true,
	},
})

brands.sync()

module.exports = brands
