const Sequelize = require('sequelize')
const { sequelize } = require('../../db/sequelize')

const messages = sequelize.define('messages', {
	rentalId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'rentals',
			key: 'id',
		},
	},
	rentalTransactionId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'rentalTransactions',
			key: 'id',
		},
	},
	senderId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'users',
			key: 'id',
		},
	},
	receiverId: {
		type: Sequelize.BIGINT,
		references: {
			model: 'users',
			key: 'id',
		},
	},
	key: {
		type: Sequelize.ENUM,
		values: [
			'chat',
			'following',
			'like',
			'requestPending',
			'requestDeclined',
			'requestCanceled',
			'requestTimout',
			'requestAccepted',
			'paymentPending',
			'paymentAuthorized',
			'paymentTimout',
			'paymentFailed',
			'paymentSuccess',
			'confirmed',
			'deliveryWaitingForLoaner',
			'deliveryShippedByLoaner',
			'inPersonCollectionPending',
			'receivedByBorrower',
			'extensionRequestPending',
			'extensionRequestDeclined',
			'extensionRequestCanceled',
			'extensionRequestAccepted',
			'extensionPaymentPending',
			'extensionPaymentFailed',
			'extensionPaymentSuccess',
			'extensionConfirmed',
			'returnWaitingForBorrower',
			'returnShippedByBorrower',
			'inPersonReturnPending',
			'receivedByLoaner',
			'completed',
		],
		allowNull: false,
	},
	message: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	isRead: {
		type: Sequelize.BOOLEAN,
		defaultValue: false,
	},
	status: {
		type: Sequelize.BOOLEAN,
		defaultValue: true,
	},
})

messages.sync()

module.exports = messages
