const Address = require('./schema/Address')
const Bannerimages = require('../models/schema/Bannerimages')
const Brands = require('./schema/Brands')
const Categories = require('./schema/Categories')
const Colors = require('./schema/Colors')
const Follows = require('./schema/Follows')
const Likes = require('./schema/Likes')
const Materials = require('./schema/Materials')
const Messages = require('./schema/Messages')
const Rentals = require('./schema/Rentals')
const Rentalimages = require('./schema/Rentalimages')
const Rentalitems = require('./schema/Rentalitems')
const Rentaltransactions = require('./schema/Rentaltransactions')
const Rentaltransactionstates = require('./schema/Rentaltransactionstates')
const Reports = require('./schema/Reports')
const Sizegroups = require('./schema/Sizegroups')
const Sizes = require('./schema/Sizes')
const Subcategories = require('./schema/Subcategories')
const User = require('./schema/User')

//users
User.hasMany(Address, { foreignKey: 'userId' })
Address.belongsTo(User, { foreignKey: 'userId' })
User.hasMany(Follows, { foreignKey: 'userId', as: 'following' })
User.hasMany(Follows, { foreignKey: 'followerUserId', as: 'follower' })
User.hasMany(Likes, { foreignKey: 'userId' })
User.hasMany(Messages, { foreignKey: 'senderId', as: 'sender' })
User.hasMany(Messages, { foreignKey: 'receiverId', as: 'receiver' })
User.hasMany(Rentals, { foreignKey: 'userId', as: 'userRental' })
User.hasMany(Rentaltransactions, { foreignKey: 'loanerId', as: 'loaner' })
User.hasMany(Rentaltransactions, { foreignKey: 'borrowerId', as: 'borrower' })
User.hasMany(Reports, { foreignKey: 'userId', as: 'reporter' })
User.hasMany(Reports, { foreignKey: 'reportedUserId', as: 'reportedUsers' })
Follows.belongsTo(User, { foreignKey: 'userId', as: 'following' })
Follows.belongsTo(User, { foreignKey: 'followerUserId', as: 'follower' })
Likes.belongsTo(User, { foreignKey: 'userId' })
Messages.belongsTo(User, { foreignKey: 'senderId', as: 'sender' })
Messages.belongsTo(User, { foreignKey: 'receiverId', as: 'receiver' })
Rentals.belongsTo(User, { foreignKey: 'userId', as: 'userRental' })
Rentaltransactions.belongsTo(User, { foreignKey: 'loanerId', as: 'loaner' })
Rentaltransactions.belongsTo(User, { foreignKey: 'borrowerId', as: 'borrower' })

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'senderId', as: 'senderReport' })
Reports.belongsTo(Messages, { foreignKey: 'reportedUserId' })

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'receiverId', as: 'receiverReport' })
Reports.belongsTo(Messages, { foreignKey: 'reportedUserId' })
Messages.hasMany(Follows, { foreignKey: 'userId', sourceKey: 'senderId', as: 'senderFollow' })
Follows.belongsTo(Messages, { foreignKey: 'userId', as: 'senderFollow' })

Messages.hasMany(Follows, { foreignKey: 'userId', sourceKey: 'receiverId', as: 'receiverFollow' })
Follows.belongsTo(Messages, { foreignKey: 'userId', as: 'receiverFollow' })
Messages.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'receiverId' })
Reports.belongsTo(Messages, { foreignKey: 'userId', sourceKey: 'userId' })

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'receiverId' })
Reports.belongsTo(Messages, { foreignKey: 'userId' })

Reports.belongsTo(User, { foreignKey: 'userId', as: 'reporter' })
Reports.belongsTo(User, { foreignKey: 'reportedUserId', as: 'reportedUsers' })

//Address
Address.hasMany(Rentals, { foreignKey: 'addressId' })
Rentals.belongsTo(Address, { foreignKey: 'addressId' })
Address.hasMany(Rentaltransactions, { foreignKey: 'borrowerAddressId' })
Rentaltransactions.belongsTo(Address, { foreignKey: 'borrowerAddressId' })
Address.hasMany(Rentaltransactions, { foreignKey: 'loanerAddressId' })
Rentaltransactions.belongsTo(Address, { foreignKey: 'loanerAddressId' })

//Rentals
Rentals.hasMany(Likes, { foreignKey: 'rentalId' })
Likes.belongsTo(Rentals, { foreignKey: 'rentalId' })
Rentals.hasMany(Messages, { foreignKey: 'rentalId' })
Messages.belongsTo(Rentals, { foreignKey: 'rentalId' })
Rentals.hasMany(Rentalimages, { foreignKey: 'rentalId' })
Rentalimages.belongsTo(Rentals, { foreignKey: 'rentalId' })
Rentals.hasMany(Rentalitems, { foreignKey: 'rentalId' })
Rentalitems.belongsTo(Rentals, { foreignKey: 'rentalId' })
Rentals.hasMany(Rentaltransactions, { foreignKey: 'rentalId' })
Rentaltransactions.belongsTo(Rentals, { foreignKey: 'rentalId' })
Rentals.hasMany(Reports, { foreignKey: 'reportedRentalId', as: 'rentalReport' })
Reports.belongsTo(Rentals, { foreignKey: 'reportedRentalId', as: 'rentalReport' })

//Rentaltransactions
Rentaltransactions.hasMany(Messages, { foreignKey: 'rentalTransactionId', as: 'messageWithRentalTransaction' })
Messages.belongsTo(Rentaltransactions, { foreignKey: 'rentalTransactionId', as: 'messageWithRentalTransaction' })
Rentaltransactions.hasMany(Rentaltransactionstates, { foreignKey: 'rentalTransactionsId' })
Rentaltransactionstates.belongsTo(Rentaltransactions, { foreignKey: 'rentalTransactionsId' })

//Sizegroups
Sizegroups.hasMany(Sizes, { foreignKey: 'sizegroupId' })
Sizes.belongsTo(Sizes, { foreignKey: 'sizegroupId' })

//Categories
Categories.hasMany(Subcategories, { foreignKey: 'categoryId' })
Subcategories.belongsTo(Subcategories, { foreignKey: 'categoryId' })
Categories.hasMany(Rentalitems, { foreignKey: 'categoryId' })
Rentalitems.belongsTo(Categories, { foreignKey: 'categoryId' })

// Subcategories
Subcategories.hasMany(Rentalitems, { foreignKey: 'subcategoryId' })
Rentalitems.belongsTo(Subcategories, { foreignKey: 'subcategoryId' })

//Brands
Brands.hasMany(Rentalitems, { foreignKey: 'brandId' })
Rentalitems.belongsTo(Brands, { foreignKey: 'brandId' })

//Colors
Colors.hasMany(Rentalitems, { foreignKey: 'colorId' })
Rentalitems.belongsTo(Colors, { foreignKey: 'colorId' })

//Materials
Materials.hasMany(Rentalitems, { foreignKey: 'materialId' })
Rentalitems.belongsTo(Materials, { foreignKey: 'materialId' })

module.exports = {
	Address,
	Brands,
	Bannerimages,
	Categories,
	Colors,
	Follows,
	Likes,
	Materials,
	Messages,
	Rentals,
	Rentalimages,
	Rentalitems,
	Rentaltransactions,
	Rentaltransactionstates,
	Reports,
	Sizegroups,
	Sizes,
	Subcategories,
	User,
}
